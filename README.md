<!-- required -->
# OilPrice


<!-- required -->
## Table of Contents
- [Description](#description) <!-- required -->
- [Requirements](#requirements) <!-- required -->
- [Payloads](#payloads) <!-- required -->
- [Constraints](#constraints) <!-- required -->
- [Changelog](#changelog) <!-- optional -->

<!-- optional -->

<!-- required -->
## Description

When this application started retrieve data from calling the endpoint that contains the JSON file of Oil Prices history and store it on IEnumerable.
When the HTTP method is called the list of result will be filtered from the body's date.
FYI : If End Date it's not valorized the default value will be assigned.

RUN THE APPLICATION 

You can choose to run this application in http (port 5000) or in https (port 5001).
The ports can be configurated in "launchSettings.json"

SEND REQUEST WITH POSTMAN
``` postman request
Url : [POST] https:\\localhost:5001\OilPrice
```

The body :

```json
{
    "id": 1,
    "jsonrpc": "2.0",
    "method": "GetOilPriceTrend",
    "params": {
        "startDateISO8601": "2020-01-01",
        "endDateISO8601": "2020-01-05"
    }
}
```

<!-- required -->
## Requirements

Implement an application which aims to provide the historical trend of oil price.
The application shall provide a JSON-RPC method named GetOilPriceTrend over HTTP.

The method shall accept the following parameters:

startDateISO8601 Date (ISO 8601 format) The starting date of the period to retrieve data
endDateISO8601 Date (ISO 8601 format) The ending date of the period to retrieve data 

The response should be a list of data points representing the price for each day included in the period.
Historical price of Europe Brent at [this address](https://pkgstore.datahub.io/core/oil-prices/brent-day_json/data/b26a150f66f90717c7e533ecc468baef/brent-day_json.json).

### Payloads

#### Request
Example of request payload.
```json
{
    "id": 1,
    "jsonrpc": "2.0",
    "method": "GetOilPriceTrend",
    "params": {
        "startDateISO8601": "2020-01-01",
        "endDateISO8601": "2020-01-05"
    }
}
```

#### Response
Example of response payload.
```json
{
    "jsonrpc": "2.0",
    "id": 1,
    "result": {
        "prices": [
            {
                "dateISO8601": "2020-01-01",
                "price": 12.3
            },
            {
                "dateISO8601": "2020-01-02",
                "price": 13.4
            },
            {
                "dateISO8601": "2020-01-03",
                "price": 14.5
            },
            {
                "dateISO8601": "2020-01-04",
                "price": 16.7
            },
            {
                "dateISO8601": "2020-01-05",
                "price": 18.9
            }
        ]
    }
}
```

### Constraints
* Use C#, VB.Net
* Publish your project to an accessible git source code repository
* Provide clear instructions on how to run your project through a README file

Note: There is no limit on usage of frameworks.




<!-- optional -->
## Changelog
See [changelog](./CHANGELOG.md).

