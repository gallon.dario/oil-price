﻿using OilPrice.Models;
using OilPrice.Services.Contract;
using System;
using System.Globalization;
using System.Reflection;

namespace OilPrice
{
    /// <summary>
    /// Service that check if the payload is valid
    /// </summary>
    public class PayloadIntegrityService : IPayloadIntegrityService
    {
        #region Params
        #endregion

        #region Constructor
        public PayloadIntegrityService( )
        {}
        #endregion

        #region Method
        public bool isValidDate(ParamsModel paramsItem)
        {
            if (paramsItem == null)
                return false;
            if (paramsItem.StartDateISO8601 == null)
                return false;
            try
            {
                DateTime dtStart = DateTime.ParseExact(paramsItem.StartDateISO8601, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                DateTime dtEnd = DateTime.ParseExact(paramsItem.EndDateISO8601, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                if (dtStart > dtEnd)
                    return false;
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Error parsing date:  {ex}");
                return false;
            }
           
            return true;
        }
        /// <summary>
        /// Method that check if id is valid
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true if > 0</returns>
        public bool isValidId(int id) => id > 0;
        /// <summary>
        /// Method that check if is valid method called
        /// </summary>
        /// <param name="method"></param>
        /// <returns>true if is legal method, otherwise false</returns>
        public bool isValidMethod(string method)
        {
            switch (method)
            {
                case "GetOilPriceTrend": return true;
                default: return false;
            }

        }

        /// <summary>
        /// Method that check if is valid version
        /// </summary>
        /// <param name="jsonRpc"></param>
        /// <returns>true if the varsion is 2.0</returns>
        public bool isValidVersion(string jsonRpc)
        {
            switch (jsonRpc)
            {
                case "2.0": return true;
                default: return false;
            }
        }
        #endregion

    }
}
