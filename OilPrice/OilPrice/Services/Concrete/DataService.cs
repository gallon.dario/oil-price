﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OilPrice.Models;
using OilPrice.Services.Contract;
using Polly;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace OilPrice
{
    /// <summary>
    /// The data managment service
    /// </summary>
    public class DataService : IDataService
    {
        #region Parameter    
        /// <summary>
        /// The configuration
        /// </summary>
        private readonly IConfiguration _configuration;
        /// <summary>
        /// Url to call
        /// </summary>
        private readonly IOptions<OilPriceDataModel> _priceDataModel;
        /// <summary>
        /// Num max of attempt
        /// </summary>
        private readonly IOptions<RetryPolicyModel> _retryPolicyAttempt;
        /// <summary>
        /// The Http Client needed for getting data
        /// </summary>
        private readonly IHttpClientFactory _httpClient;
        /// <summary>
        /// Retry policy async manager
        /// </summary>
        private readonly AsyncRetryPolicy<IEnumerable<PriceModel>> _retryPolicy;
        public IEnumerable<PriceModel> _listOfPrices;
        #endregion

        #region Constructor
        public DataService(
            IConfiguration configuration,
            IOptions<OilPriceDataModel> priceDataModel,
            IOptions<RetryPolicyModel> retryPolicyAttempt,
            IHttpClientFactory httpClient
            )
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _priceDataModel = priceDataModel ?? throw new ArgumentNullException(nameof(priceDataModel));
            _retryPolicyAttempt = retryPolicyAttempt ?? throw new ArgumentNullException(nameof(retryPolicyAttempt));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _retryPolicy = Policy<IEnumerable<PriceModel>>.Handle<HttpRequestException>().RetryAsync(_retryPolicyAttempt.Value.MaxAttempt);
            _listOfPrices = RetrieveDataAsync().Result;

        }
        #endregion
        /// <summary>
        /// Getting the oil prices datas
        /// </summary>
        /// <param name="model"></param>
        /// <returns>A model with a list of prices filtered by data</returns>
        #region Method
        public OilPriceResultModel GetData(OilPricePayloadModel model)
        {
            try
            {
                if (_listOfPrices.Any())
                {
                    DateTime dtStart = DateTime.ParseExact(model.Params.StartDateISO8601, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    DateTime dtEnd = DateTime.ParseExact(model.Params.EndDateISO8601, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                    return new OilPriceResultModel
                    {
                        Id = model.Id,
                        JsonRpc = model.JsonRpc,
                        Result = new ResultModel
                        {
                            Prices = _listOfPrices.Where(m =>
                        DateTime.Parse(m.Date, CultureInfo.InvariantCulture) >= dtStart &&
                        DateTime.Parse(m.Date, CultureInfo.InvariantCulture) <= dtEnd).ToList()
                        }
                    };
                }

                return new OilPriceResultModel
                {
                    Id = model.Id,
                    JsonRpc = model.JsonRpc,
                    Result = new ResultModel
                    {
                        Prices = new List<PriceModel>()
                    }
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting data : {ex}");
                return null;
            }
        }
        /// <summary>
        /// This method retrieve data from Http Client;
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PriceModel>> RetrieveDataAsync()
        {
            try
            {
                if (string.IsNullOrEmpty(_priceDataModel.Value.Url))
                    return null;

                List<PriceModel> ListFiltered = new List<PriceModel>();
                var client = _httpClient.CreateClient();
                client.BaseAddress = new Uri(_priceDataModel.Value.Url);

                return await _retryPolicy.ExecuteAsync(async () =>
                {
                    var response = await client.GetAsync(client.BaseAddress);
                    if (!response.IsSuccessStatusCode)
                        return null;

                    return JsonSerializer.Deserialize<IEnumerable<PriceModel>>(response.Content.ReadAsStringAsync().Result);

                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting data : {ex}");
                return null;
            }
        }
        #endregion
    }
}
