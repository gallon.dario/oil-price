﻿using OilPrice.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OilPrice.Services.Contract
{
    /// <summary>
    /// Data Service Interface
    /// </summary>
    public interface IDataService
    {
        OilPriceResultModel GetData(OilPricePayloadModel model);
        Task<IEnumerable<PriceModel>> RetrieveDataAsync();
    }
}
