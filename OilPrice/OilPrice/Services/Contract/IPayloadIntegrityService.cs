﻿using OilPrice.Models;

namespace OilPrice.Services.Contract
{
    /// <summary>
    /// Payload integrity interface
    /// </summary>
    public interface IPayloadIntegrityService
    {
        bool isValidDate(ParamsModel paramsItem);
        bool isValidId(int id);
        public bool isValidMethod(string method);
        bool isValidVersion(string jsonRpc);
    }
}
