﻿using Microsoft.AspNetCore.Mvc;
using OilPrice.Models;
using OilPrice.Services.Contract;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OilPrice.Controllers
{
    /// <summary>
    /// The web controller for Oil Price
    /// </summary>

    [ApiController]
    [Route("[controller]")]
    public class OilPriceController : ControllerBase
    {
        #region Parameter
        /// <summary>
        /// Service that check for payload integrity
        /// </summary>
        private readonly IPayloadIntegrityService _integrityService;
        /// <summary>
        /// Service that retrieve data from a payload
        /// </summary>
        private readonly IDataService _dataService;
        #endregion

        public OilPriceController(
            IPayloadIntegrityService integrityService,
            IDataService dataService
            )
        {
            _integrityService = integrityService ?? throw new ArgumentNullException(nameof(integrityService));
            _dataService = dataService ?? throw new ArgumentNullException(nameof(dataService));
        }

        /// <summary>
        /// Method that returns the oil prices between two date
        /// </summary>
        /// <param name="_model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] OilPricePayloadModel _model) 
        {

            if (!_integrityService.isValidMethod(_model.Method)) 
            {
                return StatusCode(400,
                    new OilPriceErrorResultModel
                    {
                        Id = _model.Id,
                        Result = new ErrorResultModel
                        {
                            code = (int)JRCPErrors.MethodNotFound,
                            message = "Method does not exist"
                        }

                    });
            }

            if (!_integrityService.isValidId(_model.Id))
            {
                return StatusCode(400,
                    new OilPriceErrorResultModel
                    {
                        Id = _model.Id,
                        Result = new ErrorResultModel
                        {
                            code = (int)JRCPErrors.InternalError,
                            message = "Id not valid"
                        }

                    });
            }

            if (!_integrityService.isValidVersion(_model.JsonRpc))
            {
                return StatusCode(400,
                    new OilPriceErrorResultModel
                    {
                        Id = _model.Id,
                        Result = new ErrorResultModel
                        {
                            code = (int)JRCPErrors.InvalidRequest,
                            message = "Wrong Version"
                        }

                    });
            }

            if (!_integrityService.isValidDate(_model.Params))
            {
                return StatusCode(400,
                    new OilPriceErrorResultModel
                    {
                        Id = _model.Id,
                        Result = new ErrorResultModel
                        {
                            code = (int)JRCPErrors.InvalidParam,
                            message = "Invalid Request Parameters"
                        }

                    });
            }

            try
            {
               var data = _dataService.GetData(_model);
                return Ok(data);
            }
            catch(Exception ex)
            {
                return StatusCode(500,
                   new OilPriceErrorResultModel
                   {
                       Id = _model.Id,
                       Result = new ErrorResultModel
                       {
                           code = (int)JRCPErrors.InternalError,
                           message = ex.Message
                       }

                   });
            }
            
        }
    }
}
