﻿namespace OilPrice.Models
{
    /// <summary>
    ///the JRCP Errors
    /// </summary>
    public enum JRCPErrors
    {
        InvalidRequest = -32600,
        MethodNotFound = -32601,
        InvalidParam = -32602,
        InternalError = -32603,
        ParseError = -32700
    }
}
