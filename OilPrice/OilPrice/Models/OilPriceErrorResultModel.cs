﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace OilPrice.Models
{
    /// <summary>
    /// The oil price error response data model
    /// </summary>
    public class OilPriceErrorResultModel
    {
        /// <summary>
        /// Json RPC Standard
        /// </summary>
        public string JsonRpc { get; set; } = "2.0";
        /// <summary>
        /// Id
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// Result
        /// </summary>
        public ErrorResultModel Result { get; set; }
    }
    /// <summary>
    /// The error result data model
    /// </summary>
    public class ErrorResultModel
    {
        /// <summary>
        /// Error Code
        /// </summary>
        public int code { get; set; }
        /// <summary>
        /// Error message
        /// </summary>
        public string message { get; set; }
    }
}
