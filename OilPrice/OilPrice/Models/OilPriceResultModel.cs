﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace OilPrice.Models
{
    /// <summary>
    /// The oil price response data model
    /// </summary>
    public class OilPriceResultModel
    {
        /// <summary>
        /// Json RPC Standard
        /// </summary>
        /// </summary>
        public string JsonRpc { get; set; } = "2.0";
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Result
        /// </summary>
        public ResultModel Result { get; set; }
    }
    /// <summary>
    /// The result data model
    /// </summary>
    public class ResultModel
    {
        /// <summary>
        /// List of Prices
        /// </summary>
        public List<PriceModel> Prices { get; set; }
    }
    /// <summary>
    /// The Price data model
    /// </summary>
    public class PriceModel
    {
        /// <summary>
        /// Date of the price
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Price value
        /// </summary>
        [JsonPropertyName("Brent Spot Price")]
        public decimal Price { get; set; }
    }
}
