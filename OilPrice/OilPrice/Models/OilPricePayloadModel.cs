﻿using System;
using System.Globalization;

namespace OilPrice.Models
{
    /// <summary>
    /// The model of payload
    /// </summary>
    public class OilPricePayloadModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Json RPC standard
        /// </summary>
        public string JsonRpc { get; set; } = "2.0";
        /// <summary>
        /// Name of the method
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// Params
        /// </summary>
        public ParamsModel Params { get; set; }

    }

    /// <summary>
    /// The params data model
    /// </summary>
    public class ParamsModel
    {
        /// <summary>
        /// Start date
        /// </summary>
        public string StartDateISO8601 { get; set; }
        /// <summary>
        /// End date
        /// </summary>
        public string EndDateISO8601 { get; set; } = DateTime.UtcNow.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

    }
}
