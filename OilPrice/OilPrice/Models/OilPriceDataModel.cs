﻿namespace OilPrice.Models
{
    /// <summary>
    /// the data url data model of oil price
    /// </summary>
    public class OilPriceDataModel
    {
        /// <summary>
        /// Url value
        /// </summary>
        public string Url { get; set; }
    }
}
