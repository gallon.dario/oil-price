﻿namespace OilPrice.Models
{
    /// <summary>
    /// Model of Retry Policy
    /// </summary>
    public class RetryPolicyModel
    {
        /// <summary>
        /// Num Max Attempt
        /// </summary>
        public int MaxAttempt { get; set; } = 5;
    }
}
