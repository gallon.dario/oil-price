using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OilPrice.Models;
using OilPrice.Services;
using OilPrice.Services.Contract;

namespace OilPrice
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddHttpClient();
            services.Configure<OilPricePayloadModel>(Configuration.GetSection("OilPricePayloadModel"));
            services.Configure<OilPriceDataModel>(Configuration.GetSection("OilPriceDataModel"));
            services.Configure<RetryPolicyModel>(Configuration.GetSection("RetryPolicy"));

            services.AddSingleton<IDataService, DataService>();
            services.AddSingleton<IPayloadIntegrityService, PayloadIntegrityService>(); 
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
